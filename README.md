## Instagram Parser From HAR to JSON

    The execution file (*.jar) placed at the 'Downloads' section.

##### Get a HAR files.
1.  Open Instagram followers and scroll down a few times;
2.  At the 'Network' tab of your browser click by the right button and choose `Save all as HAR with content`;
3.  And save this file (like `www.instagram.com.har`).

##### Parsing HAR
*  Put `instagramFollowersParcerFromHAR2JSON.jar` file and HAR file `www.instagram.com.har` to the disk `C:\`;
*  At the command line input the next: ` java -jar instagramFollowersParcerFromHAR2JSON.jar "www.instagram.com.har" "users.json";`
    *  or few of files: `java -jar instagramFollowersParcerFromHAR2JSON.jar "www.instagram.com.har,www.instagram1.com.har,www.instagram2.com.har" "users.json";`
*  Press Enter button;
*  `users.json` file should be displayed at the disk `C:`