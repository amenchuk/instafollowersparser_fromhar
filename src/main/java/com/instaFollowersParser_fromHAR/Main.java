package com.instaFollowersParser_fromHAR;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        String filePaths[] = null;
        String outputFileName = null;
        int recordsCountInFile = 2000;
        if (args.length <= 0) {
            System.err.println(
                    "[ERROR] Input command in format:\njava -jar instagramFollowersParcerFromHAR2JSON.jar {path_to_har} {path_to_json}\n"
                            + "Example: java -jar instagramFollowersParcerFromHAR2JSON.jar \"www.instagram.com.har\" \"users.json\"");
            System.exit(1);
        }

        try {
            filePaths = args[0].split(",");
            outputFileName = args[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println(
                    "[ERROR] Input command in format:\njava -jar instagramFollowersParcerFromHAR2JSON.jar {path_to_har} {path_to_json}\n"
                            + "Example: java -jar instagramFollowersParcerFromHAR2JSON.jar \"www.instagram.com.har\" \"users.json\"");
            System.exit(1);
        }

        JSONArray usersArray = parceToArray(filePaths);
        int fileNumber = 0;
        JSONArray usersForFile = new JSONArray();
        for (int i = 1; i <= usersArray.size(); i++) {
            if ((i % recordsCountInFile) != 0) {
                usersForFile.add(usersArray.get(i - 1));
            }
            if (((i % recordsCountInFile) == 0) || (i == usersArray.size())) {
                fileNumber++;
                JSONObject outputJson = new JSONObject();
                outputJson.put("parseDate", new SimpleDateFormat("HH:mm:ss yyyy.MM.dd").format(new Date()));
                outputJson.put("file_name", outputFileName + "_" + fileNumber + ".json");
                outputJson.put("userCount", usersForFile.size() + 1);
                outputJson.put("users", usersForFile);
                writeToFileJson(outputFileName + "_" + fileNumber + ".json", outputJson);
                System.err.println("[INFO] File '" + outputFileName + "_" + fileNumber + ".json' was created. Parsed " + (usersForFile.size()+1) + " users.");
                usersForFile.clear();
            }
        }
        System.err.println("[INFO] FINISH!");
    }

    public static JSONArray parceToArray(String[] pathToFile) throws IOException, ParseException {
        JSONArray usersJSON = new JSONArray();
        JSONParser parser = new JSONParser();
        List<Object> usersFromContent = null;
        Object file = null;

        for (String filePath : pathToFile) {
            System.err.println("[INFO] Parsing file: " + filePath);
            try {
                file = parser.parse(new FileReader(filePath));
            } catch (FileNotFoundException e) {
                System.err.println("The HAR file not found. Check path to HAR file.");
            }
            List<Object> listOfContents = JsonPath.read(file, "$.log.entries[*].response.content.text");

            for (Object content : listOfContents) {
                if (content.toString().matches(".+data.+user.+edge_followed_by.*")) {
                    file = parser.parse(content.toString());
                    try {
                        usersFromContent = JsonPath.read(file, "$.data.user.edge_followed_by.edges[*].node");

                        for (Object user : usersFromContent) {
                            Object document =
                                    Configuration.defaultConfiguration().jsonProvider().parse(user.toString());

                            String id = JsonPath.read(document, "$.id");
                            String full_name = JsonPath.read(document, "$.full_name");
                            String username = JsonPath.read(document, "$.username");
                            String isPrivate = String.valueOf(JsonPath.read(document, "$.is_private"));
                            String is_verified = String.valueOf(JsonPath.read(document, "$.is_verified"));
                            String requested_by_viewer =
                                    String.valueOf(JsonPath.read(document, "$.requested_by_viewer"));
                            String followed_by_viewer =
                                    String.valueOf(JsonPath.read(document, "$.followed_by_viewer"));

                            if (full_name.toLowerCase().contains("магазин") || full_name.toLowerCase().contains("store") ||
                                    full_name.toLowerCase().contains("кондиционер") || full_name.toLowerCase().contains("ремонт") ||
                                    full_name.toLowerCase().contains("salon")) continue;
                            Map<String, String> userFields = new HashMap();
                            userFields.put("id", id);

                            userFields.put("full_name", full_name.replaceAll("[^A-Za-zА-Яа-я]", ""));

                            userFields.put("username", username);
                            userFields.put("isPrivate", isPrivate);
                            userFields.put("is_verified", is_verified);
                            userFields.put("requested_by_viewer", requested_by_viewer);
                            userFields.put("followed_by_viewer", followed_by_viewer);

                            usersJSON.add(JSONObject.toJSONString(userFields));
                        }
                    } catch (PathNotFoundException e) {
                        e.getMessage();
                    }
                }
            }
        }
        return usersJSON;
    }

    public static void writeToFileJson(String fileName, JSONObject json) throws IOException {
        FileWriter file = new FileWriter(fileName);
        file.write(
                json.toJSONString()
                        .replaceAll("\\\\(\\\")", "$1")
                        .replaceAll("\\\"(\\{)", "$1")
                        .replaceAll("(\\})\\\"", "$1"));
        file.flush();
    }
}
